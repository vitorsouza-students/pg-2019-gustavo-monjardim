% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Este capítulo apresenta os principais conceitos teóricos que fundamentaram o
desenvolvimento do Portal de Biodiversidade de Chalcidoidea e está organizado em 4 seções. A Seção~\ref{sec-ref-chalcidoidea} apresenta a superfamília Chalcidoidea. A Seção~\ref{sec-ref-chaves} define o que são chaves de identificação.  A Seção~\ref{sec-ref-engenharia} aborda os principais conceitos e processos utilizados na Engenharia de Software. A Seção~\ref{sec-ref-web} apresenta conceitos importantes de desenvolvimento Web. Por fim, a Seção~\ref{sec-ref-frameweb} apresenta o método FrameWeb.

%%% Início de seção. %%%
\section{Chalcidoidea}
\label{sec-ref-chalcidoidea}


Chalcidoidea é uma das várias superfamílias de vespas conhecidas como ``parasitoides'', ou seja, cujo ciclo de vida inclui uma fase larval que se desenvolve dentro de ou sobre outro organismo, normalmente outro inseto, que é consumido para a emergência da vespa adulta. Ao contrário das vespas mais popularmente conhecidas, elas não constroem ninhos nem possuem ferrão. Como pode ser visto na Figura~\ref{fig-chalcidoidea}, são extremamente diversas morfologicamente, a maioria com 0,5 a 10mm de comprimento, sendo que este grupo inclui os menores insetos conhecidos (0,13mm). Estima-se que existam mais de 500.000 espécies, das quais, somente 22.506 já foram descritas~\cite{chalcidoidea}.

Chalcidoidea representa uma porção significante da diversidade biológica encontrada em ecossistemas terrestres e desempenham um papel fundamental na regulagem da população de insetos terrestres. A maioria dessas vespas são parasitas de outros insetos e são extensivamente usadas no controle biológico de pragas de insetos. De fato, a maioria dos projetos de controle biológico de pragas de insetos bem sucedidos utilizaram vespas Chalcidoidea para obter controle substancial ou completo~\cite{gibson1997}. Assim, estas espécies possuem uma importância ecológica e econômica muito grande.

\begin{figure}[h]
	\centering
	\includegraphics[width=.75\textwidth]{./figuras/chalcidoidea.jpg} 
	\caption{Diversidade morfológica de Chalcidoidea~\cite{chalcidoidea}.}
	\label{fig-chalcidoidea}
\end{figure}


Devido a sua abundância, distribuição mundial, interações biológicas complexas, diversidade morfológica, e a pequena quantidade de taxonomistas que a estudam, ainda há muita pesquisa a ser desenvolvida sobre essa superfamília.


%%% Início de seção. %%%
\section{Chaves de Identificação}
\label{sec-ref-chaves}

Uma chave taxonômica é basicamente uma série de perguntas sobre as características de um organismo desconhecido, essas perguntas são chamadas de \textbf{caracteres} da chave de identificação. Cada caractere oferece uma lista de alternativas que o usuário pode selecionar como resposta à pergunta em questão, sendo tais alternativas conhecidas como os \textbf{estados} desse caractere. Ao responder as perguntas, o usuário é levado à identificação do organismo desconhecido, encontrando o táxon ao qual ele pertence. Para isso, a chave de identificação também possui uma lista de todos os possíveis organismos que podem ser identificados por essa chave, esses organismos são chamados de \textbf{entidades} da chave de identificação.

Essas chaves são dividas em duas categorias: chaves de acesso único, também conhecidas como chaves sequenciais, e chaves de múltiplo acesso, também conhecidas como chaves de múltipla entrada ou policlaves~\cite{winston-1999}.

Tradicionalmente, chaves de identificação em sua maior parte são desenvolvidas como chaves de acesso único. Nesse tipo de chave é oferecida uma sequência fixa de passos de identificação, cada um com múltiplas alternativas, sendo que a escolha de uma dessas alternativas leva ao próximo passo. Quando cada passo possui apenas duas alternativas para o usuário escolher, a chave é chamada de dicotômica, caso contrário, ela é uma chave politômica~\cite{wiki:keys}. Como nesse tipo de chave deve-se começar a partir do primeiro passo e seguir uma ordem determinada até chegar ao fim, o progresso pode ser interrompido quando o usuário não sabe a resposta em um dos passos. Dessa forma o resultado final não é obtido.  

Por outro lado, as chaves de múltiplo acesso ou chaves interativas, permitem que o usuário escolha em qual passo de identificação deseja começar, podendo também pular os passos na qual a resposta não é conhecida.

Uma chave interativa é um programa de computador na qual o usuário informa os atributos (valores de estado-caractere) do organismo. Tipicamente, o programa elimina os táxons que possuem atributos diferentes dos atributos informados pelo usuário. O processo continua até o usuário informar atributos suficientes para sobrar apenas um táxon, identificando assim o organismo desconhecido~\cite{delta:2018}. As principais vantagens que chaves interativas possuem em relação às chaves convencionais são: 


\begin{itemize}
	\item Qualquer caractere pode ser acessado e seus valores podem ser modificados, em qualquer ordem;
	
	\item Uma identificação correta pode ser feita mesmo com a existência de erros do usuário ou dos dados; 
	
	\item Caracteres numéricos podem ser usados diretamente, sem a necessidade de serem divididos em classes;
	
	\item O usuário pode expressar incerteza, selecionando mais de um estado para um caractere.
\end{itemize}


Na taxonomia, um caractere deve prover de forma clara e facilmente observável uma característica distinta que ajude a identificar um organismo. A escolha de caracteres na hora de elaborar uma chave de identificação deve ter como objetivo facilitar a identificação correta do organismo na menor quantidade de passos possível.

%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-ref-engenharia}

O uso de softwares tornou-se um aspecto muito presente em todas as áreas de nossas vidas e, consequentemente, o número de pessoas interessadas nos recursos e nas funções oferecidas por uma determinada aplicação tem crescido significativamente. Quando uma aplicação está sendo desenvolvida, muitas pessoas devem ser ouvidas, e, no geral, cada uma delas possui uma ideia diferente de quais funções ou recursos o software deve oferecer. Dessa forma, deve-se dedicar um esforço concentrado para compreender o problema antes de desenvolver uma solução de software~\cite{Pressman2011}. Visando melhorar a qualidade dos produtos de software e aumentar a produtividade no processo de desenvolvimento, surgiu a Engenharia de Software~\cite{falbo:book14}.

A Engenharia de Software é a área da Ciência da Computação voltada à especificação, desenvolvimento e manutenção de sistemas de software, com aplicação de tecnologias e práticas de gerência de projetos e outras disciplinas, visando organização, produtividade e qualidade no processo de software~\cite{falbo:book14}. 

No livro \textit{How to Solve it}, \citeonline{Polya1945} definiu quatro etapas para a solução de problemas. Consequentemente, essas 4 etapas são a essência da Engenharia de Software:

\begin{enumerate}
	\item Compreender o problema (comunicação e análise);
	
	\item Planejar uma solução (modelagem e projeto de software);
	
	\item Executar o plano (codificação do sistema);
	
	\item Examinar o resultado para ter precisão (testes e garantia da qualidade).
\end{enumerate}

Dessa forma, os métodos da Engenharia de Software envolvem uma ampla gama de tarefas, que incluem: comunicação, análise de requisitos, modelagem de projeto, construção de programa, testes e suporte~\cite{Pressman2011}.


%%% Início de subseção. %%%
\subsection{Especificação e Análise de Requisitos}
\label{sec-ref-engenharia-requisitos}

Requisitos são descrições do que o sistema deve fazer, as funcionalidades que o mesmo oferece e as restrições às quais está submetido. Os requisitos expressam as necessidades dos clientes que irão utilizar o sistema desenvolvido. O processo de descobrir, analisar, documentar e verificar essas funcionalidades e restrições é chamado Engenharia de Requisitos~\cite{Sommerville2011}.

A Engenharia de Requisitos têm um papel fundamental no desenvolvimento de software, pois um software é considerado bem sucedido quando atende os requisitos determinados para a sua construção. Requisitos estão presentes ao longo de todo o ciclo de vida de um software, servindo de base para a modelagem, projeto, implementação, testes e até mesmo para a manutenção do sistema~\cite{falbo:book17}.

Segundo \citeonline{Sommerville2011}, os requisitos de software são dividos em dois tipos, os requisitos funcionais e os requisitos não-funcionais. Dessa forma, temos que:

\begin{itemize}
	\item \textbf{Requisitos funcionais}: são especificações das funcionalidades que o sistema deve fornecer, de como o sistema deve reagir a entradas específicas e de como o sistema deve se comportar em determinadas situações. Em alguns casos, os requisitos funcionais também podem explicitar o que o sistema não deve fazer.	
	
	\item \textbf{Requisitos não funcionais}: são restrições sobre as funções oferecidas pelo sistema, tais como restrições de tempo, de uso de recursos, entre outras. De maneira geral, requisitos não funcionais referem-se a atributos de qualidade que o sistema deve apresentar, tais como confiabilidade, usabilidade, eficiência, portabilidade, manutenibilidade e segurança. 
	
\end{itemize}

O processo de Engenharia de Requisitos começa pelo levantamento de requisitos, atividade na qual devem ser coletadas informações sobre as necessidades dos usuários e clientes, informações do domínio, sistemas existentes, regulamentos, leis, etc. Uma vez identificados os requisitos, é iniciada a etapa de análise, quando os requisitos levantados são usados como base para a modelagem do sistema. Tanto no levantamento quanto na análise de requisitos é importante documentar requisitos e modelos. Para documentar requisitos, dois documentos são normalmente utilizados: o \textbf{Documento de Definição de Requisitos} e o \textbf{Documento de Especificação de Requisitos}. No Documento de Definição de Requisitos se encontra a descrição do minimundo, na qual é apresentada, em forma de texto corrido, uma visão geral do domínio, do problema a ser resolvido e dos processos de negócio apoiados, além das principais ideias e sugestões do cliente sobre o sistema a ser desenvolvido. Nele também se encontra a lista dos requisitos de cliente identificados. Já no Documento de Especificação de Requisitos são registrados os requisitos de sistema e os vários diagramas resultantes do trabalho de análise. Os documentos produzidos são, então, verificados e validados~\cite{falbo:book17}.

De acordo com \citeonline{falbo:book17}, a fase de análise de requisitos, em sua essência, é uma atividade de modelagem. A modelagem nesta fase se preocupa apenas com o domínio do problema e não com soluções técnicas para o mesmo, por isso é chamada de modelagem conceitual. Os modelos de análise são elaborados para se obter um entendimento maior acerca do sistema a ser desenvolvido. Diferentes modelos podem ser construídos para representar diferentes perspectivas, dentre eles podemos citar o diagrama de classes e o modelo de entidades e relacionamentos, que buscam modelar os conceitos, propriedades e relações do domínio que são relevantes para o sistema em desenvolvimento. Além disso, também são utilizados os diagramas de casos de uso, diagramas de atividades, diagramas de estados e diagramas de interação para modelar o comportamento geral do sistema, de suas funcionalidades ou de uma entidade específica ao longo do tempo.

%%% Início de subseção. %%%
\subsection{Projeto e Implementação}
\label{sec-ref-engenharia-projeto}

O objetivo da fase de projeto é produzir uma solução para o problema identificado e modelado nas fases de levantamento e análise de requisitos, incorporando a tecnologia aos requisitos e projetando o que será construído na implementação. Sendo assim, é necessário conhecer a tecnologia disponível e os ambientes de hardware e software onde o sistema será desenvolvido e implantado. Durante o projeto, deve-se decidir como o problema será resolvido, começando em um alto nível de abstração, próximo da análise, e progredindo sucessivamente para níveis mais detalhados até se chegar a um nível de abstração próximo da implementação~\cite{falbo:book16}. O projeto de software encontra-se no núcleo técnico do processo de desenvolvimento de software e é aplicado independentemente do modelo de ciclo de vida e paradigma adotados. É iniciado assim que os requisitos do software tiverem sido modelados e especificados pelo menos parcialmente e é a última atividade de modelagem. Por outro lado, corresponde à primeira atividade que leva em conta aspectos tecnológicos~\cite{Pressman2011}.

Partindo dos modelos conceituais do sistema, a fase de projeto inicia-se com o projeto da arquitetura do sistema, que visa definir os elementos estruturais do software e seus relacionamentos. Feito isso, o projeto passa a se concentrar no detalhamento de cada um desses elementos, o que envolve a decomposição de módulos em outros módulos menores. Uma vez especificado o projeto dos elementos da arquitetura, pode dar-se início à implementação, quando as unidades de software do projeto detalhado são implementadas e testadas individualmente (teste de unidade). Aos poucos, cada elemento da arquitetura vai sendo integrado e testado (teste de integração), até se obter o sistema completo. Por fim, deve ser feito um teste completo do sistema (teste de sistema)~\cite{falbo:book16}.


De acordo com \citeonline{Pressman2011}, a importância do projeto de software pode ser definida em uma única palavra: qualidade. A fase de projeto é a etapa em que a qualidade é incorporada na Engenharia de Software. O projeto nos fornece representações de software que podem ser avaliadas em termos de qualidade. Na fase de projeto é onde podemos traduzir precisamente os requisitos dos interessados em um software finalizado.


%%% Início de seção. %%%
\section{Desenvolvimento Web}
\label{sec-ref-web}

Nos primórdios da World Wide Web (por volta de 1990 a 1995), os sites eram formados por apenas conjuntos de arquivos de hipertexto que apresentavam informações usando texto e gráficos limitados. Com o surgimento de novas ferramentas de desenvolvimento, como o XML e o Java, tornou-se possível aos engenheiros da Internet oferecerem capacidade computacional juntamente com as informações. Dessa forma, nasceram os sistemas e aplicações baseados na Web~\cite{Pressman2011}.

Dois atributos-chave distinguem o desenvolvimento de softwares baseados na Web do desenvolvimento de softwares tradicionais: o crescimento rápido de seus requisitos e a velocidade alta com o qual o seu seu conteúdo muda. Com isso, aplicações Web precisam ser planejadas para serem escaláveis e de fácil manutenção, uma vez que tais recursos não podem ser adicionados posteriormente. O sucesso na construção, desenvolvimento, implementação e manutenção de um sistema Web depende do quão bem essas questões foram tratadas~\cite{Ginige2001}.

%%% Início de subseção. %%%
\subsection{O Ambiente Web}
\label{sec-ref-web-ambiente}

De acordo com \citeonline{mai-pg17}, o ambiente Web é sustentado por 3 componentes principais: o protocolo de comunicação HyperText Transfer Protocol (HTTP), o sistema de endereçamento Uniform Resource Locator (URL) e a linguagem HyperText Markup Language (HTML).

O protocolo HTTP é o ingrediente mais básico sobre o qual a Web está fundamentada. Ele é um protocolo de aplicações cliente-servidor que define um formato padrão para especificar a requisição de recursos na Web. Através dele, o cliente (navegador ou dispositivo que fará a requisição) pode requisitar recursos disponíveis a um servidor remoto. Como esses recursos estão distribuídos ao longo de toda a Internet, é necessário um mecanismo de identificação que permita localizá-los e acessá-los. O identificador usado para referenciar esses recursos é uma URL~\cite{Casteleyn2009}.

Segundo \citeonline{Casteleyn2009}, ao acessar uma URL, o cliente envia uma requisição HTTP, sendo que esta requisição possui um cabeçalho especificando um método HTTP. Os métodos HTTP mais utilizados são:

\begin{itemize}
	\item \textbf{GET}: solicita um determinado recurso. Requisições GET devem ser usadas apenas para recuperar dados  e não devem ter qualquer outro efeito;
	
	\item \textbf{POST}: envia dados para serem processados para o recurso especificado. Os dados são incluídos no corpo da requisição;
	
	\item \textbf{PUT}: envia os dados de forma semelhante ao POST, porém é utilizado para atualizar dados já existentes no servidor;
	
	\item \textbf{DELETE}: utilizado para excluir um recurso do servidor.
	
\end{itemize}

Além desses métodos, também existem os métodos HEAD, OPTIONS, TRACE e CONNECT.

Ao receber uma requisição, o servidor localiza o recurso solicitado e envia uma resposta para o cliente. Essa resposta inclui o seu status, um número que indica se a requisição feita pelo cliente foi bem sucedida ou não. De acordo com \citeonline{http-rfc}, o código de status é formado por três dígitos, sendo que o primeiro dígito representa a categoria a qual a resposta pertence. Uma resposta pode ser classificada em cinco categorias:

\begin{itemize}
	\item \textbf{1xx:} \textit{Informational} (Informação): utilizada para informar o cliente que sua requisição foi recebida e está sendo processada;
	
	\item \textbf{2xx:} \textit{Success} (Sucesso): indica que a requisição do cliente foi bem sucedida;
	
	\item \textbf{3xx:} \textit{Redirection} (Redirecionamento): informa a ação adicional que deve ser tomada para completar a requisição;
	
	\item \textbf{4xx:} \textit{Client Error} (Erro no cliente): o cliente fez uma requisição que não pode ser atendida;
	
	\item \textbf{5xx:} \textit{Server Error} (Erro no servidor): ocorreu um erro no servidor ao cumprir uma requisição válida.
	
\end{itemize}

Para expressar o conteúdo e a formatação visual das páginas Web é utilizada a linguagem HTML. O HTML é uma linguagem de marcação, ou seja, um documento HTML é composto por \textit{tags} que delimitam um certo conteúdo e definem como o mesmo deve ser representando visualmente. O navegador recebe como entrada o conteúdo marcado, interpreta o significado das \textit{tags} e transforma o conteúdo recebido em um documento renderizado~\cite{Casteleyn2009}.

Complementar ao HTML, o CSS (Folha de Estilo em Cascatas, do inglês \textit{Cascading Style Sheets}) é utilizado para definir a aparência de uma página Web, separando o conteúdo HTML da sua representação visual. Uma folha de estilos é um conjunto de regras que definem como o navegador deve apresentar o documento. Um regra é composta de duas partes: um seletor, que define para qual \textit{tag} HTML a regra deve ser aplicada, e uma declaração de estilo, que define as propriedades que devem ser adicionadas a tag HTML mencionada no seletor~\cite{Casteleyn2009}.

%%% Início de subseção. %%%
\subsection{O padrão MVC de Arquitetura de Software}
\label{sec-ref-web-mvc}

A arquitetura Modelo-Visão-Controlador (MVC) é um dos vários padrões de arquitetura sugeridos para aplicações Web~\cite{Pressman2011}. Esse padrão é uma das soluções mais avançadas propostas pela Engenharia de Software para a necessidade de modularização, já que foi desenvolvido com a escalabilidade e a separação de conceitos em mente~\cite{Casteleyn2009}. 

As noções de separação e independência entre componentes são essenciais para o projeto de arquitetura, pois permitem que alterações sejam feitas de forma localizada, sem comprometer outros componentes. O padrão MVC separa os elementos de uma aplicação, permitindo modificá-los de forma independente~\cite{Sommerville2011}.

Nesse padrão o sistema é separado em três camadas~\cite{Fowler2002}. Na camada \textit{Modelo} se encontram todos os dados do sistema e as ações associadas a esses dados. Essa camada pode ser compartilhada entre diversas aplicações. O Modelo ignora como requisições de usuários são feitas e como os dados são apresentados para o usuário. A camada \textit{Visão} contém todas as funções específicas à interface com o usuário e define como os dados do modelo serão exibidos. Por fim, a camada \textit{Controlador} coordena o fluxo de dados entre o modelo e a visão. O Controlador recebe requisições do usuário, manipula o Modelo e atualiza a Visão de acordo com as modificações executadas. 

Na Figura~\ref{fig-web-mvc} é possível ver o comportamento do padrão MVC. Nele, os dados ou requisições de usuários são tratados pelo controlador, que também seleciona qual componente da Visão exibir baseado na requisição do usuário. Uma vez determinado o tipo da requisição recebida, o Controlador transmite uma solicitação de comportamento ao Modelo, que executa uma ação ou recupera os dados necessários para atender à requisição. Para fazer isso, o Modelo pode acessar dados armazenados em um banco de dados externo. Os dados retornados pelo Modelo devem ser formatados e organizados pela Visão para serem exibidos de forma apropriada na máquina do usuário que fez a requisição.


\begin{figure}[h]
	\centering
	\includegraphics[width=.85\textwidth]{./figuras/MVC-Pressman.png} 
	\caption{A arquitetura MVC~\cite{Pressman2011}.}
	\label{fig-web-mvc}
\end{figure}



%%% Início de subseção. %%%
\subsection{Node.js e o Framework Express.js}
\label{sec-ref-web-node}


O Node.js é um ambiente de execução assíncrono orientado a eventos construído sobre o motor JavaScript do Google Chrome (Engine V8), ele foi criado com o objetivo de permitir a construção de aplicações de rede escaláveis. Node.js usa um modelo de E/S (Entrada e Saída) orientado a eventos e não bloqueante, o que o torna leve e eficiente, ideal para aplicações em tempo real com troca intensa de dados executadas em dispositivos distribuídos~\cite{node-js}.

A principal característica de um servidor feito com Node.js é o fato de sua execução ser \textit{single-threaded} com chamadas de E/S não-bloqueantes, enquanto outros servidores tradicionais possuem uma execução \textit{multi-threaded} com chamadas bloqueantes. Na prática isso significa que em um servidor tradicional, para cada requisição recebida é criada uma nova \textit{thread} para tratá-la. Já em um servidor Node.js, apenas uma \textit{thread} é responsável por tratar as requisições. Essa \textit{thread}, conhecida como \textit{Event Loop} fica em execução aguardando a chegada de novos eventos. Quando um determinado código emite um evento, o mesmo é enviado para a fila de eventos para que o \textit{Event Loop} o execute. Diferentemente dos servidores tradicionais, a \textit{thread} não fica esperando que as operações que serão executadas sejam concluídas, pois essas operações serão executadas através de chamadas não-bloqueantes, e o mecanismo de notificação de eventos do Node.js ficará responsável por notificar o servidor quando a resposta dessas operações for obtida.

O Node.js, porém, não possui nativamente suporte a algumas tarefas comuns do desenvolvimento Web. O tratamento específico para métodos HTTP, especificação de rotas para requisições, uso de \textit{templates} para criar páginas HTML dinamicamente, são algumas das funções que não existem nativamente no Node.js. A forma mais fácil de obtê-las é utilizando um \textit{framework}. 

O Express.js é o \textit{framework} mais popular do Node.js. Ele é um \textit{framework} minimalista e flexível que fornece um conjunto robusto de recursos para o desenvolvimento de aplicações Web. Complementar a esses recursos, desenvolvedores criaram uma série de pacotes de \textit{middleware} que adereçam basicamente qualquer necessidade existente no desenvolvimento Web. Existem pacotes para se trabalhar com \textit{cookies}, sessões, autenticação de usuários, segurança, \textit{upload} de arquivos e muito mais~\cite{mdn-node}.

Dessa forma, utilizando o Express.js, é possível definir rotas para receber requisições de usuários, utilizar uma \textit{template engine} para renderizar páginas HTML dinamicamente, conectar com um banco de dados para armazenar os dados da aplicação. Tais características possibilitam o desenvolvimento de um sistema Web robusto.

%%% Início de seção. %%%
\section{O Método FrameWeb}
\label{sec-ref-frameweb}

O FrameWeb (\textit{Framework-based Design Method for Web Engineering}) é um método de projeto para construção de sistemas de informação Web utilizando \textit{frameworks}. Ele é baseado em metodologias e linguagens de modelagens bastante utilizadas na área de Engenharia de Software~\cite{souza:masterthesis07}. 

O método, sugere um processo de software orientado a objetos contendo as fases de levantamento de requisitos, análise, projeto, codificação, testes e implantação, mas podendo ser estendido e adaptado pela equipe de desenvolvimento. A linguagem de modelagem UML (\textit{Unified Modeling Language}) é utilizada durante todas as etapas do processo. Na fase de levantamento de requisitos, é sugerida a elaboração de modelos de casos de uso. Para a fase de análise, é sugerido o desenvolvimento de diagramas de classes com alto nível de abstração, construindo um modelo conceitual do domínio do problema~\cite{souza:masterthesis07}. 

O foco do método FrameWeb, porém, são as fases de projeto e implementação. É durante a fase de projeto que a plataforma onde o software será desenvolvido e implantado é levada em consideração. São feitas, portanto, as seguintes propostas: (i) definição de uma arquitetura de software que divida o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) elaboração de um conjunto de modelos que utilizam conceitos utilizados por \textit{frameworks}. Para isso é proposto um perfil UML que possibilite que os diagramas fiquem mais próximos da implementação do sistema~\cite{souza:masterthesis07}.

Dessa forma, o FrameWeb propõe uma arquitetura lógica baseada no padrão arquitetônico \textit{Service Layer} definido por~\citeonline{Fowler2002} e que pode ser visto na Figura~\ref{fig-arquiteturaFrameweb}. A primeira camada dessa arquitetura possui os pacotes Visão e Controle, responsáveis por apresentar dados na interface e receber requisições vindas do usuário. A segunda camada é responsável pela lógica de negócio da aplicação e possui os pacotes Domínio e Aplicação, responsáveis por armazenar as classes que representam conceitos do domínio do problema e por implementar os casos de uso definidos na especificação de requisitos, respectivamente. Por fim, a terceira e última camada é responsável pela lógica de acesso a dados, contém apenas o pacote Persistência, que é responsável pela persistência dos dados.

\begin{figure}[h]
	\centering
	\includegraphics[width=.80\textwidth]{./figuras/arquitetura-frameweb.png} 
	\caption{Arquitetura proposta pelo FrameWeb~\cite{souza:masterthesis07}.}
	\label{fig-arquiteturaFrameweb}
\end{figure}

A partir da arquitetura definida, é proposto uma extensão da linguagem UML~\cite{martins-souza:webmedia15} que possibilita a construção de quatro tipos de diagramas:

\begin{itemize}
	\item \textbf{Modelo de Entidades} (antes Modelo de Domínio): um diagrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional. Nele, o modelo de classes construído na fase de análise de requisitos é adequado para à plataforma de implementação escolhida. Para isso, os tipos de dados de cada atributo são indicados, a navegabilidade das associações é definida, entre outras ações. Além disso, são adicionados os mapeamentos de persistência, meta-dados das classes de domínio que permitem que os \textit{frameworks} ORM (\textit{Object/Relational Mapping}) transformem objetos que estão na memória em linhas de tabelas no banco de dados relacional e vice-versa. Estereótipos e restrições definidas pela linguagem FrameWeb guiam os desenvolvedores na configuração do \textit{framework} ORM. Apesar de tais configurações serem relacionadas mais à persistência do que ao domínio, elas são representadas no Modelo de Entidades porque as classes que são mapeadas e seus atributos são exibidas neste diagrama~\cite{souza:masterthesis07}. A partir desse modelo são implementadas as classes da camada \emph{Domínio} da arquitetura proposta pelo FrameWeb.
	
	\item \textbf{Modelo de Persistência}: um diagrama de classes da UML que representa as classes DAO existentes, que pertencem ao pacote \emph{Persistência}, responsáveis pela persistência das instâncias das classes de domínio. Nesse modelo são apresentados, para cada classe de domínio que necessita de lógica de acesso a dados, uma interface e uma classe concreta DAO que a implementa. A interface define os métodos de persistência existentes para aquela classe, tais métodos são implementados por uma ou mais classes concretas;
	
	\item \textbf{Modelo de Navegação}: um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e classes controladoras. Esse modelo é utilizado pelos desenvolvedores para guiar a codificação das classes e componentes dos pacotes \emph{Visão} e \emph{Controle}. Nele é possível identificar como os dados são submetidos pelo usuário e quais resultados são possíveis a partir de uma certa entrada de dados;
	
	\item \textbf{Modelo de Aplicação}: um diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências. Esse diagrama é utilizado para guiar a implementação das classes do pacote Aplicação e a configuração das dependências entre os pacotes \emph{Controle}, \emph{Aplicação} e \emph{Persistência}, ou seja, quais classes controladoras dependem de quais classes de serviço e quais DAOs são necessários para que as classes de serviço alcancem seus objetivos.
\end{itemize}
