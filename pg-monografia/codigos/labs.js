// GET all labs.
router.get('/', function(req, res, next) {
  Lab.find()
  .sort({year: -1})
  .exec().then((labs) => {
    if(labs == null){
      var err = new Error('Labs not found');
      err.status = 404;
      next(err);
    }
    res.render('labs', {labs: labs, user: req.user});
  }).catch((err) => {
    next(err);
  });
});

// Create Lab
router.post('/create', require('connect-ensure-login').ensureLoggedIn('/users/login'), [
   body('name').isLength({ min: 1 }).trim().withMessage('Name must be specified.'),
   body('city').isLength({ min: 1 }).trim().withMessage('City name must be specified.'),
   body('state').isLength({ min: 1 }).trim().withMessage('State name must be specified.'),
  ], (req, res, next) => {

  const lab = new Lab({
    name: req.body.name,
    city: req.body.city,
    state: req.body.state
  });

  lab.save(function (err) {
    if (err) { return next(err); }
      res.redirect('/dashboard');
    });
});

// Get Especific Lab
router.get('/:id', function(req, res, next) {

  Lab.findById(req.params.id)
    .exec().then((lab) => {
      if (lab==null) {
        console.log("not found");
          const err = new Error('Lab not found');
          err.status = 404;
          return next(err);
      }
      res.render('lab_detail', { title: lab.title});
    });

});

// Delete Lab
router.post('/:id/delete', require('connect-ensure-login').ensureLoggedIn('/users/login'), function(req, res, next) {
  const id = mongoose.Types.ObjectId(req.params.id);
  Lab.findByIdAndRemove(id,function(err, lab) {
    if(err) {
    next(err);
    } else {
      res.send('success');
    }
  });
});

// Update Lab
router.post('/:id/update', require('connect-ensure-login').ensureLoggedIn('/users/login'), [
   body('name').isLength({ min: 1 }).trim().withMessage('Name must be specified.'),
   body('city').isLength({ min: 1 }).trim().withMessage('City name must be specified.'),
   body('state').isLength({ min: 1 }).trim().withMessage('State name must be specified.'),
  ], (req, res, next) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  const lab = new Lab({
    _id: id,
    name: req.body.name,
    city: req.body.city,
    state: req.body.state
  });
  Lab.findByIdAndUpdate(req.params.id, lab, {}, function (err, lab) {
    if (err) {
      return next(err);
    }
    res.redirect('/dashboard');
  });

});

module.exports = router;
