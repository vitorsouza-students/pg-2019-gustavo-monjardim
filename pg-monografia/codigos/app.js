const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const session = require('express-session');
var MemoryStore = require('memorystore')(session)
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');
const helmet = require('helmet');
const compression = require('compression');
const csrf = require('csurf');


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const entitiesRouter = require('./routes/entities');
const nodesRouter = require('./routes/nodes');
const keysRouter = require('./routes/keys');
const resultsRouter = require('./routes/results');
const dashboardRouter = require('./routes/dashboard');
const publicationsRouter = require('./routes/publications');
const projectsRouter = require('./routes/projects');
const researchersRouter = require('./routes/researchers');
const labsRouter = require('./routes/labs');
const postsRouter = require('./routes/posts');
const aboutRouter = require('./routes/about');
const savesRouter = require('./routes/saves');


const app = express();

app.use(helmet());

app.use(compression());
app.use(express.static(path.join(__dirname, 'public')));

app.locals.moment = require('moment');


const mongoose = require('mongoose');
const mongoDB = 'mongodb://admin:admin@ds135810.mlab.com:35810/bioapp';
mongoose.connect(mongoDB, { useNewUrlParser: true });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(session(
{
  secret: "LaboratorioBiodiversidadeInsetos",
  store: new MemoryStore({
      checkPeriod: 60 * 60 * 1000
  }),
  name: "LaBISession",
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 60 * 60 * 1000,
    httpOnly: true,
 }
}));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


app.use(csrf());


app.use(function (req, res, next) {
  var token = req.csrfToken();
  res.cookie('X-XSRF-TOKEN', token);
  res.locals.csrfToken = token;
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/entities', entitiesRouter);
app.use('/nodes', nodesRouter);
app.use('/keys', keysRouter);
app.use('/results', resultsRouter);
app.use('/dashboard', dashboardRouter);
app.use('/publications', publicationsRouter);
app.use('/projects', projectsRouter);
app.use('/researchers', researchersRouter);
app.use('/labs', labsRouter);
app.use('/posts', postsRouter);
app.use('/saves', savesRouter);
app.use('/about', aboutRouter);

const User = require('./models/user');
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/*app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});*/

module.exports = app;
